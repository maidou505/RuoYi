新建模块：ruoyi-cms
参照：ruoyi-system

在根pom.xml文件添加
<!-- 通用工具-->
<dependency>
<groupId>com.ruoyi</groupId>
<artifactId>ruoyi-cms</artifactId>
<version>${ruoyi.version}</version>
</dependency>

modules添加
<module>ruoyi-cms</module>

在ruoyi-admin的pom.xml中添加
<!-- cms后台-->
        <dependency>
            <groupId>com.ruoyi</groupId>
            <artifactId>ruoyi-cms</artifactId>
        </dependency>