-- 字典类型
INSERT INTO `ry`.`sys_dict_type` (`dict_id`, `dict_name`, `dict_type`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, '文章类型', 'article_type', '0', 'admin', '2025-01-03 15:47:27', '', NULL, NULL);

-- 字典数据
INSERT INTO `ry`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (100, 1, '赋能套件', '1', 'article_type', NULL, 'primary', 'Y', '0', 'admin', '2025-01-03 15:48:41', '', NULL, NULL);
INSERT INTO `ry`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (101, 2, '入门教程', '2', 'article_type', NULL, 'success', 'Y', '0', 'admin', '2025-01-03 15:49:28', '', NULL, NULL);
INSERT INTO `ry`.`sys_dict_data` (`dict_code`, `dict_sort`, `dict_label`, `dict_value`, `dict_type`, `css_class`, `list_class`, `is_default`, `status`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (102, 3, '轮播图片', '3', 'article_type', '', 'warning', 'Y', '0', 'admin', '2025-01-03 15:49:53', 'admin', '2025-01-03 15:50:00', '');

-- 新增菜单
INSERT INTO `ry`.`sys_menu` (`menu_id`, `menu_name`, `parent_id`, `order_num`, `url`, `target`, `menu_type`, `visible`, `is_refresh`, `perms`, `icon`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`) VALUES (2000, '运营管理', 0, 5, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-cogs', 'admin', '2025-01-03 16:13:43', '', NULL, '');